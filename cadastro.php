<?php

    /* 
     * Criado por Eduardo dos santos (Resource)
     * Criado em 01/03/2019
     * Usado para cadastrar as APKs
    */

    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Sao_Paulo');

    $user   = isset($_POST['user'])     ? $_POST['user']    : "";
    $build  = isset($_POST['build'])    ? $_POST['build']   : "";
    $log    = isset($_POST['log'])      ? $_POST['log']     : "";

    if($user != "" && $build != "" && $log != "")
    {
        $uploaddir = 'upload/'.round(microtime(true) * 1000).rand(1,1000);
        mkdir($uploaddir , 0777);
        $uploadfile = $uploaddir . "/app.apk";
        if (is_uploaded_file($_FILES['file']['tmp_name']))
        { 
            $moved = move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
            if(!$moved) {

                echo "<script>alert('Falha ao registrar!: '".$_FILES["file"]["error"].")</script>";
                
            } else {

                $json = array(
                    'user'  => $user,
                    'build' => $build,
                    'log'   => $log,
                    'date'  => date('d/m/Y H:i:s')
                );
    
                $fp = fopen($uploaddir.'/dados.json', 'w');
                fwrite($fp, json_encode($json));
                fclose($fp);
    
                echo "<script>alert('Registrado com sucesso!')</script>";

            }

        }
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="favicon.png"/>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    <title>Upload APK</title>

</head>
<body style="background-color: #f2f2f2; padding: 30px;">

<div class="container">

  <div class="row">
    <div class="col-10 align-self-center" align="center">
        <h1>Nova APK</h1>
    </div>
    <div class="col-2 align-self-center" align="right">
        <a href="index.php" class="btn btn-outline-info">Lista</a>
    </div>
  </div>

  <br>
  
  <form enctype="multipart/form-data" action="cadastro.php" method="POST">
    <div class="row">
        <div class="col-4 align-self-center" align="center">
        <img src="upload.png" alt="">
        <br>
        Faça o Upload de sua APK aqui
        <input type="file" name="file">
        </div>
        <div class="col-8 align-self-start">
            <label for="basic-url">Desenvolvedor</label>
            <div class="input-group mb-3">
                <select class="form-control" name="user" id="user" style="width: 100%;">
                    <option value="Alex">Alex</option>
                    <option value="Jonatas">Jonatas</option>
                    <option value="Paulo">Paulo</option>
                </select>
            </div>

            <label for="basic-url">Build</label>
            <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon3">Build</span>
            </div>
                <input type="text" class="form-control" id="build" name="build" aria-describedby="basic-addon3">
            </div>

            <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Log da versão</span>
            </div>
                <textarea name="log" id="log" class="form-control" aria-label="With textarea" rows="10"></textarea>
            </div>

        </div>
    </div>
    <hr>

  <div class="row">
    <div class="col-12 align-self-end" align="right">
        <button type="submit" class="btn btn-outline-success">Enviar</button>
    </div>
  </div>

  </form>

</div>
    
</body>
</html>