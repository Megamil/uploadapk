<?php

    /* 
     * Criado por Eduardo dos santos (Resource)
     * Criado em 01/03/2019
     * Usado listar as APKs cadastradas
    */

    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    date_default_timezone_set('America/Sao_Paulo');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" type="image/png" href="favicon.png"/>

    <title>Lista de APKs</title>

</head>
<body style="background-color: #f2f2f2; padding: 30px;">
    
<div class="container">

  <div class="row">
    <div class="col-10 align-self-center" align="center">
        <h1>Lista de APKs</h1>
    </div>
    <div class="col-2 align-self-center" align="right">
        <a href="cadastro.php" class="btn btn-outline-info">Cadastrar</a>
    </div>
  </div>

  <br>  

    <div class="row">
    <table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Desenvolvedor</th>
            <th scope="col">Build</th>
            <th scope="col">Data</th>
            <th scope="col">LOG</th>
            <th scope="col">Download</th>
        </tr>
    </thead>
    <tbody>

    <?php

        $path = "upload/";
        $diretorio = dir($path);

        while($sub_diretorio = $diretorio -> read()){
            if($sub_diretorio != "." && $sub_diretorio != ".."){
                $sub_diretorio_string = $path.$sub_diretorio."/"; 

                if(is_dir($sub_diretorio_string)){
                
                    $str = file_get_contents($sub_diretorio_string."dados.json");
                    $json = json_decode($str, true);
                    ?>
                    <tr>
                        <td><?php echo $json['user']; ?></td>
                        <td><?php echo $json['build']; ?></td>
                        <td><span style="display: none;"><?php echo $sub_diretorio_string; ?></span><?php echo $json['date']; ?></td>
                        <td><?php echo $json['log']; ?></td>
                        <td align="center">
                            <a href="<?php echo $sub_diretorio_string; ?>app.apk" width="100%" align="center">
                                <img src="download.png" width="32px">
                            </a>
                        </td>
                    </tr>
                    
                    <!-- <div class="col-3 card-group">
                        <div class="card">
                            <img src="favicon.png" class="card-img-top" width="64px">
                            <div class="card-body">
                                <h5 class="card-title">Build </h5>
                                <p class="card-text">Log da versão: <?php //echo $json['log']; ?></p>
                                <p class="card-text"><small class="text-muted">Dev: <?php //echo $json['user']; ?> em <?php //echo $json['date']; ?></small></p>
                                
                                <a href="<?php //echo $sub_diretorio_string; ?>app.apk" width="100%" align="center">
                                    <img src="download.png" width="32px">
                                </a>
                            </div>
                        </div>
                    </div> -->
                    
            <?php }
            
            }

        }
        $diretorio -> close();

    ?>  

        </tbody>
    </table>

</div>
    
</body>
<footer>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>

    <script>
        $(document).ready( function () {
                    // Adicionando um campos com input para as buscas indivíduais.
         $('table thead th').each( function () {
                var title = $(this).text();
                if (title != '' && !$(this).hasClass('no-filter')) {
                    $(this).html(title+'<br><div class="form-group has-feedback"><input type="text" class="form-control" placeholder="'+title+'" alt="'+title+'" title="'+title+'"/><i class="glyphicon glyphicon-search form-control-feedback"></i></div>' );    
                }
            } );

            var table = $('table').DataTable({
                "oLanguage": {
                    "sLengthMenu": "Mostrar _MENU_ registros por página",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sInfo": "Mostrando _START_ / _END_ de _TOTAL_ registro(s)",
                    "sInfoEmpty": "Mostrando 0 / 0 de 0 registros",
                    "sInfoFiltered": "(filtrado de _MAX_ registros)",
                    "sSearch": "Pesquisa Geral: ",
                    "oPaginate": {
                        "sFirst": "Início",
                        "sPrevious": "Anterior",
                        "sNext": "Próximo",
                        "sLast": "Último"
                    }
                },
                "order": [[ 2, "desc" ]]
            });
                     
         
            //Aplicando a busca nos campos
            table.columns().every( function () {
                var that = this;
         
                $( 'input', this.header() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        });
    </script>
</footer>
</html>